import { Injectable } from '@angular/core';
import { Weapon } from './pages/models/weapon';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { fromEvent } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class WeaponService {
  private urlAddition = 'weapons'; // URL to web api

  constructor(
    private http: HttpClient) {
  }

  /** GET heroes from the server */
  getWeapons(): Observable<Weapon[]> {
    return this.http.get<Weapon[]>(`${environment.apiUrl}/${this.urlAddition}`)
      .pipe(
        catchError(this.handleError<Weapon[]>('getWeapons', []))
      );
  }

  /** GET heroes from the server */
  getWeapon(id: string): Observable<Weapon[]> {
    return this.http.get<Weapon[]>(`${environment.apiUrl}/${this.urlAddition}/${id}`)
      .pipe(
        catchError(this.handleError<Weapon[]>('getWeapon', []))
      );
  }

  /** GET heroes from the server */
  postWeapon(weapon: Weapon): Observable<Weapon> {
    const body = {
      'name': weapon.name,
      'description': weapon.description,
      'hp': weapon.hp,
      'atk': weapon.atk,
      'spd': weapon.spd
    }
    return this.http.post<Weapon>(`${environment.apiUrl}/${this.urlAddition}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Weapon>('postWeapon'))
      );
  }

  /** GET heroes from the server */
  patchWeapon(weapon: Weapon): Observable<Weapon> {
    const body = {
      'name': weapon.name,
      'description': weapon.description,
      'hp': weapon.hp,
      'atk': weapon.atk,
      'spd': weapon.spd
    }
    return this.http.patch<Weapon>(`${environment.apiUrl}/${this.urlAddition}/${weapon._id}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Weapon>('patchWeapon'))
      );
  }

  /** GET heroes from the server */
  deleteWeapon(id: string): Observable<Weapon> {
    return this.http.delete<Weapon>(`${environment.apiUrl}/${this.urlAddition}/${id}`, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Weapon>('deleteWeapon'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private httpOptionCreator() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("bearerToken")}`
      }),
    };
  }
}
