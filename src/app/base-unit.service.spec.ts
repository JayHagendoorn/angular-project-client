import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { BaseUnitService } from './base-unit.service';
import { HeroTemplate } from "./pages/models/hero-template";

describe('BaseUnitService', () => {
  let baseUnitService: BaseUnitService;
  let httpSpy: {get: jasmine.Spy, post: jasmine.Spy, patch: jasmine.Spy, delete: jasmine.Spy};
  let alertSpy: any;
  let baseUnitServiceSpy: any;
  let routerSpy: any;

  beforeEach(() => {
    alertSpy = jasmine.createSpyObj('AlertService',
      ['error', 'success']);
    httpSpy = jasmine.createSpyObj('HttpClient',
      ['get', 'post', 'patch', 'delete', 'pipe']);
    baseUnitServiceSpy = jasmine.createSpyObj('BaseUnitService',
      ['getBaseUnits', 'getHeroTemplate', 'postTemplate', 'patchTemplate', 'deleteTemplate']);
    routerSpy = jasmine.createSpyObj('Router',
      ['navigateByUrl']);

    baseUnitService = new BaseUnitService(httpSpy as any);

  });

  fit('should be created', () => {
    expect(baseUnitService).toBeTruthy();
  });

  fit('should get all templates after getBaseUnits()', () => {
    const expectedTemplates: HeroTemplate[] = [
      {
        _id: "1a",
        name: "NewTest",
        title: "NewTitle",
        weaponType: "Lance",
        movementType: "Infantry",
        hp: 2,
        atk: 2,
        spd: 2
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));
    const subs = baseUnitService.getBaseUnits().subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplates);
    });

    subs.unsubscribe();
  });

  fit('should get specific template after getHeroTemplate()', () => {
    const expectedTemplates: HeroTemplate[] = [
      {
        _id: "1a",
        name: "NewTest",
        title: "NewTitle",
        weaponType: "Lance",
        movementType: "Infantry",
        hp: 2,
        atk: 2,
        spd: 2
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));

    const subs = baseUnitService.getHeroTemplate(expectedTemplates[0]._id).subscribe(r => {
      const result = r as any;
      expect(result[0]).toEqual(expectedTemplates[0]);
    });

    subs.unsubscribe();
  });

  fit('should post template after postTemplate() w/ valid credentials', () => {
    let creatingTemplate = new HeroTemplate("Kool", "Lever", "Lance", "Infantry", 5, 21, 54);

    httpSpy.post.and.returnValue(of(creatingTemplate));

    const subs = baseUnitService.postTemplate(creatingTemplate).subscribe(r => {
      const result = r as any;
      expect(result).toEqual(creatingTemplate);
    });

    subs.unsubscribe();
  });

  fit('should patch template after patchTemplate() w/ valid credentials', () => {
//    let creatingTemplate = new HeroTemplate("Kool", "Lever", "Lance", "Infantry", 5, 21, 54);
//    let patchingTemplate: HeroTemplate;
//    let expectedName = "Stratje";
//
//    const stubs = baseUnitService.postTemplate(creatingTemplate).subscribe(r => {
//      const result = r as any;
//      patchingTemplate = result.heroTemplate;
//      patchingTemplate.name = expectedName;
//    });
//
//    const subs = baseUnitService.patchTemplate(patchingTemplate).subscribe(r => {
//      const result = r as any;
//      expect(result.heroTemplate.name).toEqual(expectedName);
//    });
//
//    stubs.unsubscribe();
//    subs.unsubscribe();
    const expectedTemplate: HeroTemplate = {
      _id: "1a",
      name: "NewTest",
      title: "NewTitle",
      weaponType: "Lance",
      movementType: "Infantry",
      hp: 2,
      atk: 2,
      spd: 2
    };
    httpSpy.patch.and.returnValue(of(expectedTemplate));

    const subs = baseUnitService.patchTemplate(expectedTemplate).subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplate);
    });

    subs.unsubscribe();
  });

  fit('should delete template after deleteTemplate() w/ valid credentials', () => {
//    let creatingTemplate = new HeroTemplate("Kool", "Lever", "Lance", "Infantry", 5, 21, 54);
//    let deletingTemplate: HeroTemplate;
//    let expectedName = creatingTemplate.name;
//
//    const stubs = baseUnitService.postTemplate(creatingTemplate).subscribe(r => {
//      const result = r as any;
//      deletingTemplate = result.heroTemplate;
//    });
//
//    const subs = baseUnitService.deleteTemplate(deletingTemplate._id).subscribe(r => {
//      const result = r as any;
//      expect(result.heroTemplate.name).toEqual(expectedName);
//    });
//
//    stubs.unsubscribe();
//    subs.unsubscribe();
    const expectedTemplate: HeroTemplate = {
        _id: "1a",
        name: "NewTest",
        title: "NewTitle",
        weaponType: "Lance",
        movementType: "Infantry",
        hp: 2,
        atk: 2,
        spd: 2
      };
    httpSpy.delete.and.returnValue(of(expectedTemplate));

    const subs = baseUnitService.deleteTemplate(expectedTemplate._id).subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplate);
    });

    subs.unsubscribe();
  });

  fit('should not delete template after deleteTemplate() w/ wrong credentials', () => {
//    let creatingTemplate = new HeroTemplate("Kool", "Lever", "Lance", "Infantry", 5, 21, 54);
//    let deletingTemplate: HeroTemplate;
//
//    const stubs = baseUnitService.postTemplate(creatingTemplate).subscribe(r => {
//      const result = r as any;
//      deletingTemplate = result.heroTemplate;
//    });
//
//    const subs = baseUnitService.deleteTemplate(deletingTemplate._id + "extra").subscribe(r => {
//      const result = r as any;
//      expect(result.heroTemplate.name).toEqual(undefined);
//    });
//
//    stubs.unsubscribe();
//    subs.unsubscribe();

    const expectedTemplate: HeroTemplate = {
      _id: "1a",
      name: "NewTest",
      title: "NewTitle",
      weaponType: "Lance",
      movementType: "Infantry",
      hp: 2,
      atk: 2,
      spd: 2
    };
    httpSpy.delete.and.returnValue(of(expectedTemplate));

    const subs = baseUnitService.deleteTemplate(expectedTemplate._id + "extra").subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplate);
    });

    subs.unsubscribe();
  });
});
