import { Injectable } from '@angular/core';
import { HeroTemplate } from './pages/models/hero-template';
//import { BASEUNITS } from './mock-data';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { fromEvent } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BaseUnitService {
  private urlAddition = 'templates';  // URL to web api

//  private httpOptions = {
//    headers: new HttpHeaders({
//      'Content-Type': 'application/json',
//      'Authorization': `Bearer ${localStorage.getItem("bearerToken")}`
//    }),
//  };
  constructor(
    private http: HttpClient) { }

  /** GET heroes from the server */
  getBaseUnits(): Observable<HeroTemplate[]> {
    return this.http.get<HeroTemplate[]>(`${environment.apiUrl}/${this.urlAddition}`);
  }

  /** GET heroes from the server */
  getHeroTemplate(id: string): Observable<HeroTemplate[]> {
    return this.http.get<HeroTemplate[]>(`${environment.apiUrl}/${this.urlAddition}/${id}`)
      .pipe(
        catchError(this.handleError<HeroTemplate[]>('getHeroTemplate', []))
      );
  }

  /** GET heroes from the server */
  postTemplate(template: HeroTemplate): Observable<HeroTemplate> {

    const body = {
      'name': template.name,
      'title': template.title,
      'weaponType': template.weaponType,
      'movementType': template.movementType,
      'hp': template.hp,
      'atk': template.atk,
      'spd': template.spd
    }
    return this.http.post<HeroTemplate>(`${environment.apiUrl}/${this.urlAddition}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<HeroTemplate>('postHero'))
      );
  }

  /** GET heroes from the server */
  patchTemplate(template: HeroTemplate): Observable<HeroTemplate> {
    const body = {
      'name': template.name,
      'title': template.title,
      'weaponType': template.weaponType,
      'movementType': template.movementType,
      'hp': template.hp,
      'atk': template.atk,
      'spd': template.spd
    }
    return this.http.patch<HeroTemplate>(`${environment.apiUrl}/${this.urlAddition}/${template._id}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<HeroTemplate>('patchHero'))
      );
  }

  /** GET heroes from the server */
  deleteTemplate(id: string): Observable<HeroTemplate> {

    return this.http.delete<HeroTemplate>(`${environment.apiUrl}/${this.urlAddition}/${id}`, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<HeroTemplate>('deleteTemplate'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      return of(result as T);
    };
  }

  private httpOptionCreator() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("bearerToken")}`
      }),
    };
  }

}
