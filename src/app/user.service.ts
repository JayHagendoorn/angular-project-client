import { Injectable } from '@angular/core';
import { User } from './pages/models/user';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { fromEvent } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlAddition = 'users'; // URL to web api

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient) {
  }

  /** GET heroes from the server */
  login(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.apiUrl}/${this.urlAddition}/login`, {username, password})
      .pipe(
        catchError(this.handleError<User>('login'))
      );
  }

  /** GET heroes from the server */
  register(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.apiUrl}/${this.urlAddition}/register`, { username, password })
      .pipe(
        catchError(this.handleError<User>('getWeapon'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
