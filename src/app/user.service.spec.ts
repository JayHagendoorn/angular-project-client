import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UserService } from './user.service';

describe('UserService', () => {
  let userService: UserService;
  let httpSpy: { get: jasmine.Spy, post: jasmine.Spy, patch: jasmine.Spy, delete: jasmine.Spy };
  let alertSpy: any;
  let userServiceSpy: any;
  let routerSpy: any;

  const expectedUsername: string = "admin";

  beforeEach(() => {
    alertSpy = jasmine.createSpyObj('AlertService',
      ['error', 'success']);
    httpSpy = jasmine.createSpyObj('HttpClient',
      ['get', 'post', 'patch', 'delete', 'pipe']);
    userServiceSpy = jasmine.createSpyObj('UserService',
      ['login', 'register']);
    routerSpy = jasmine.createSpyObj('Router',
      ['navigateByUrl']);

    userService = new UserService(httpSpy as any);

//    TestBed.configureTestingModule({});
//    service = TestBed.inject(UserService);
  });

  fit('should be created', () => {
    expect(userService).toBeTruthy();
  });

  fit('should log in user after login() w/ valid credentials', () => {
//    const username = 'admin';
//    const password = 'admin';
//
//    const subs = userService.login(username, password).subscribe(r => {
//      const result = r as any;
//      expect(result.info.username).toEqual("admin");
//    });
//
//    subs.unsubscribe();

    const expectedUsername: string = "TestMan";
    const expectedPassword: string = "PassMan";

    httpSpy.post.and.returnValue(of({ expectedUsername, expectedPassword}));
    const subs = userService.login(expectedUsername, expectedPassword).subscribe(r => {
      const result = r as any;
      expect(result).toEqual({expectedUsername, expectedPassword});
    });

    subs.unsubscribe();
  });

  fit('should register user after register() w/ valid credentials', () => {
//    const username = 'tony';
//    const password = 'bassworld';
//
//    const subs = userService.register(username, password).subscribe(r => {
//      const result = r as any;
//      expect(result.info.username).toEqual("tony");
//    });
//
    //    subs.unsubscribe();
    const expectedUsername: string = "TestMan";
    const expectedPassword: string = "PassMan";

    httpSpy.post.and.returnValue(of({ expectedUsername, expectedPassword }));
    const subs = userService.register(expectedUsername, expectedPassword).subscribe(r => {
      const result = r as any;
      expect(result).toEqual({ expectedUsername, expectedPassword });
    });

    subs.unsubscribe();
  });
});
