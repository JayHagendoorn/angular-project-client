import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from "../../user.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  isNavbarCollapsed = true;
  user = null;
  userId = null;
  adminId = environment.adminId;

  constructor(private userService: UserService,
    private router: Router) {}

  ngOnInit(): void {
    this.user = localStorage.getItem("AUsername");
    this.userId = localStorage.getItem("AUserId");
  }

  logout() {
    localStorage.removeItem("bearerToken");
    localStorage.removeItem("AUsername");
    localStorage.removeItem("AUserId");
    this.user = localStorage.getItem("AUsername");
    this.userId = localStorage.getItem("AUserId");
    this.router.navigateByUrl('/');
  }
}
