import { Injectable } from '@angular/core';
import { Hero } from './pages/models/hero';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { fromEvent } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private urlAddition = 'heroes'; // URL to web api
  private userUrlAddition = 'users/heroes'; // URL to web api

  constructor(
    private http: HttpClient) {
  }

  /** GET heroes from the server */
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(`${environment.apiUrl}/${this.userUrlAddition}`, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Hero[]>('getHeroes', []))
      );
  }

  /** GET heroes from the server */
  getHero(id: string): Observable<Hero> {
    return this.http.get<Hero>(`${environment.apiUrl}/${this.urlAddition}/${id}`)
      .pipe(
        catchError(this.handleError<Hero>('getHero'))
      );
  }

  /** GET heroes from the server */
  getAllHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(`${environment.apiUrl}/${this.urlAddition}`)
      .pipe(
        catchError(this.handleError<Hero[]>('getAllHeroes', []))
      );
  }

  /** GET heroes from the server */
  postHero(template: string, weapon: string): Observable<Hero> {
    const body = {
      'template': template,
      'weapon': weapon
    }

//    console.log(body);
    return this.http.post<Hero>(`${environment.apiUrl}/${this.urlAddition}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Hero>('postHero'))
      );
  }

  /** GET heroes from the server */
  patchHero(id: string, weapon: string): Observable<Hero> {
    const body = {
      'weapon': weapon
    }

    return this.http.patch<Hero>(`${environment.apiUrl}/${this.urlAddition}/${id}`, body, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Hero>('patchHero'))
      );
  }

  /** GET heroes from the server */
  deleteHero(id: string): Observable<Hero> {
    return this.http.delete<Hero>(`${environment.apiUrl}/${this.urlAddition}/${id}`, this.httpOptionCreator())
      .pipe(
        catchError(this.handleError<Hero>('patchHero'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private httpOptionCreator() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("bearerToken")}`
      }),
    };
  }
}
