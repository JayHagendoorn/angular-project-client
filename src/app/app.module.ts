import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { UsecaseComponent } from './pages/about/usecases/usecase/usecase.component';
import { BaseUnitComponent } from "./pages/base-unit/base-unit.component";
import { WeaponComponent } from "./pages/weapon/weapon.component";
import { UserHeroComponent } from './pages/user-hero/user-hero.component';
import { CreateHeroComponent } from './pages/create-hero/create-hero.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './shared/pipes/filter.pipe';
import { HeroListAllComponent } from './pages/hero-list-all/hero-list-all.component';
import { HeroDetailComponent } from './pages/hero-detail/hero-detail.component';
import { CreateTemplateComponent } from './pages/hero-template/create-template/create-template.component';
import { UpdateTemplateComponent } from './pages/hero-template/update-template/update-template.component';
import { CreateWeaponComponent } from './pages/weapon-action/create-weapon/create-weapon.component';
import { UpdateWeaponComponent } from './pages/weapon-action/update-weapon/update-weapon.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    DashboardComponent,
    UsecaseComponent,
    UsecasesComponent,
    LoginComponent,
    RegisterComponent,
    BaseUnitComponent,
    WeaponComponent,
    UserHeroComponent,
    CreateHeroComponent,
    FilterPipe,
    HeroListAllComponent,
    HeroDetailComponent,
    CreateTemplateComponent,
    UpdateTemplateComponent,
    CreateWeaponComponent,
    UpdateWeaponComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
