import { Component, OnInit } from '@angular/core';

import { Hero } from "../models/hero";
import { HeroService } from '../../hero.service';

@Component({
  selector: 'app-user-hero',
  templateUrl: './user-hero.component.html',
  styleUrls: ['./user-hero.component.css']
})
export class UserHeroComponent implements OnInit {

  constructor(private heroService: HeroService) { }

  heroes: Hero[];

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(r => {
        const result = r as any;
        this.heroes = result.heroes;
      });
  }

}
