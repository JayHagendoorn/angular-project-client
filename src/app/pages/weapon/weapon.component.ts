import { Component, OnInit } from '@angular/core';

import { Weapon } from '../models/weapon';
import { WeaponService } from '../../weapon.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-weapon',
  templateUrl: './weapon.component.html',
  styleUrls: ['./weapon.component.css']
})
export class WeaponComponent implements OnInit {

  constructor(private weaponService: WeaponService,
  private router: Router) {}

  weapons: Weapon[];

  userId = null;
  adminId = environment.adminId;

  ngOnInit() {
    this.getWeapons();
    this.userId = localStorage.getItem("AUserId");
  }

  getWeapons(): void {
    this.weaponService.getWeapons()
      .subscribe(r => {
        const result = r as any;
        this.weapons = result.weapons;
      });
  }

  patchWeapon(id: string): void {
    this.router.navigateByUrl(`/weapons/${id}`);
  }

  deleteWeapon(id: string): void {
    this.weaponService.deleteWeapon(id)
      .subscribe(r => {
        const result = r as any;
        this.getWeapons();
      });
  }
}
