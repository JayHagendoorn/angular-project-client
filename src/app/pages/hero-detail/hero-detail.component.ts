import { Component, OnInit } from '@angular/core';
import { Hero } from "../models/hero";
import { HeroService } from "../../hero.service";
import { UserService } from "../../user.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Weapon } from "../models/weapon";
import { WeaponService } from "../../weapon.service";

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  constructor(private heroService: HeroService,
    private userService: UserService,
    private weaponService: WeaponService,
    private route: ActivatedRoute,
    private router: Router) { }

  hero: Hero;
  weapons: Weapon[];

  selectedWeapon: Weapon;

  ngOnInit(): void {
    this.getHero();
    this.getWeapons();
  }

  getHero(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(r => {
        const result = r as any;
        this.hero = result.hero;
        this.selectedWeapon = result.hero.weapon;
      });
  }

  getWeapons(): void {
    this.weaponService.getWeapons()
      .subscribe(r => {
        const result = r as any;
        this.weapons = result.weapons;
      });
  }

  patchHero(): void {
    this.heroService.patchHero(this.hero._id, this.selectedWeapon._id)
      .subscribe(r => {
        const result = r as any;
        this.getHero();
        this.getWeapons();
      });
  }

  deleteHero(): void {
    this.heroService.deleteHero(this.hero._id)
      .subscribe(r => {
        const result = r as any;
        this.router.navigateByUrl(`/myheroes`);
      });
  }
}
