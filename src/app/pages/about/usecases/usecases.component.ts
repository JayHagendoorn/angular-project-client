import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';
  title = 'Feh Unit Tracker'
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'De gebruiker kan inloggen met zijn/haar account.',
      scenario: [
        'De gebruiker vult zijn/haar gebruikersnaam en wachtwoord in en klikt op Login knop.',
        'De applicatie checkt de gegeves voor validatie.',
        'De applicatie stuurt de gebruiker verder als de gegevens kloppen.'
      ],
      actor: 'Gebruiker met een account',
      precondition: 'De gebruiker heeft een account hebben',
      postcondition: 'De gebruiker is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'De gebruiker kan een account aanmaken maken.',
      scenario: [
        'De gebruiker vult zijn/haar gewenste gebruikersnaam en wachtwoord in en klikt op registreren knop.',
        'De applicatie checkt de gegevens voor validatie.',
        'De applicatie stuur de gebruiker verder als de gegevens kloppen.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker heeft geen account.',
      postcondition: 'De gebruiker heeft een account.'
    },
    {
      id: 'UC-03',
      name: 'Aanmaken nieuwe hero template',
      description: 'De admin kan een hero template maken.',
      scenario: [
        'De admin vult de naam, beschrijving, stats en types van de unit in.',
        'De applicatie checkt de gegevens voor validatie.',
        'De hero wordt toegevoegd.'
      ],
      actor: 'Admin',
      precondition: 'De admin is ingelogd.',
      postcondition: 'Een nieuwe template hero is toegevoegd.'
    },
    {
      id: 'UC-04',
      name: 'Aanmaken nieuw wapen',
      description: 'De admin kan een wapen maken.',
      scenario: [
        'De admin vult de naam, beschrijving, stats en type van de unit in.',
        'De applicatie checkt de gegevens voor validatie.',
        'Het wapen wordt toegevoegd.'
      ],
      actor: 'Admin',
      precondition: 'De admin is ingelogd.',
      postcondition: 'Een nieuw wapen is toegevoegd.'
    },
    {
      id: 'UC-05',
      name: 'Aanmaken hero',
      description: 'De gebruiker kan een hero aanmaken maken op basis van een hero template.',
      scenario: [
        'De gebruiker kiest een hero template.',
        'De gebruiker kiest alle skills een favoriet nummer indien gewenst.',
        'De applicatie checkt de gegevens voor validatie.',
        'De applicatie voegt de unit toe en koppelt het aan het account van de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'De gebruiker heeft een nieuwe unit.'
    },
    {
      id: 'UC-06',
      name: 'Bekijken gebruikers heroes',
      description: 'De gebruiker kan de heroes bekijken die hij/zij heeft gemaakt.',
      scenario: [
        'De gebruiker bekijkt de zelfgemaakte heroes.',
        'De applicatie bekijkt de gebruiker.',
        'Indien de gebruiker heroes heeft gemaakt laat de applicatie zijn/haar heroes zien.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'De zelfgemaakte heroes verschijnen op het scherm.'
    },
    {
      id: 'UC-07',
      name: 'Bekijken alle heroes',
      description: 'De gebruiker kan alle heroes bekijken die zijn gemaakt.',
      scenario: [
        'De gebruiker bekijkt de zelfgemaakte heroes.',
        'De applicatie bekijkt de gebruiker.',
        'Indien er heroes zijn gemaakt laat de applicatie alle heroes zien.'
      ],
      actor: 'Gebruiker',
      precondition: 'Niks',
      postcondition: 'Alle heroes verschijnen op het scherm.'
    },
    {
      id: 'UC-08',
      name: 'Bekijken alle hero templates',
      description: 'De gebruiker kan alle hero templates bekijken die in het systeem staan.',
      scenario: [
        'De gebruiker bekijkt de hero templates.',
        'Indien er hero templates in het systeem zijn laat de applicatie alle hero templates zien.'
      ],
      actor: 'Gebruiker',
      precondition: 'Niks.',
      postcondition: 'Alle hero templates verschijnen op het scherm.'
    },
    {
      id: 'UC-09',
      name: 'Bekijken alle wapens',
      description: 'De gebruiker kan alle wapens bekijken die in het systeem staan.',
      scenario: [
        'De gebruiker bekijkt de wapens.',
        'Indien er wapens in het systeem zijn laat de applicatie alle wapens zien.'
      ],
      actor: 'Gebruiker',
      precondition: 'Niks.',
      postcondition: 'Alle wapens verschijnen op het scherm.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
