import { Component, OnInit } from '@angular/core';
import { HeroService } from "../../hero.service";
import { WeaponService } from "../../weapon.service";
import { BaseUnitService } from "../../base-unit.service";
import { HeroTemplate } from "../models/hero-template";
import { Weapon } from "../models/weapon";
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-hero',
  templateUrl: './create-hero.component.html',
  styleUrls: ['./create-hero.component.css']
})
export class CreateHeroComponent implements OnInit {

  constructor(private heroService: HeroService,
    private weaponService: WeaponService,
    private baseUnitService: BaseUnitService,
    private router: Router) { }

  heroTemplates: HeroTemplate[];
  weapons: Weapon[];

  selectedTemplate: HeroTemplate = null;
  selectedWeapon: Weapon;

  ngOnInit(): void {
    this.getWeapons();
    this.getBaseUnits();
  }

  getWeapons(): void {
    this.weaponService.getWeapons()
      .subscribe(r => {
        const result = r as any;
        this.weapons = result.weapons;
      });
  }

  getBaseUnits(): void {
    this.baseUnitService.getBaseUnits()
      .subscribe(r => {
        const result = r as any;
        this.heroTemplates = result.heroTemplates;
      });
  }

  postHero(): void {
    this.heroService.postHero(this.selectedTemplate._id, this.selectedWeapon._id)
      .subscribe(r => {
        const result = r as any;
        this.router.navigateByUrl(`/heroes/${result.hero._id}`);
      });
  }
}
