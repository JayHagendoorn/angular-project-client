import { Component, OnInit } from '@angular/core';
import { HeroTemplate } from '../models/hero-template';
import { BaseUnitService } from '../../base-unit.service';
import { environment } from "../../../environments/environment";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-base-unit',
  templateUrl: './base-unit.component.html',
  styleUrls: ['./base-unit.component.css']
})
export class BaseUnitComponent implements OnInit {

  constructor(private baseUnitService: BaseUnitService,
    private route: ActivatedRoute,
    private router: Router) { }

  heroTemplates: HeroTemplate[];

  userId = null;
  adminId = environment.adminId;

  ngOnInit() {
    this.getBaseUnits();
    this.userId = localStorage.getItem("AUserId");
  }

  getBaseUnits(): void {
    this.baseUnitService.getBaseUnits()
      .subscribe(r => {
        const result = r as any;
        this.heroTemplates = result.heroTemplates;
      });
  }

  patchTemplate(id: string): void {
    this.router.navigateByUrl(`/templates/${id}`);
  }

  deleteTemplate(id: string): void {
    this.baseUnitService.deleteTemplate(id)
      .subscribe(r => {
        const result = r as any;
        this.getBaseUnits();
      });
  }
}
