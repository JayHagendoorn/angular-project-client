import { Component, OnInit } from '@angular/core';

import { Hero } from "../models/hero";
import { HeroService } from '../../hero.service';
import { UserService } from "../../user.service";
import { User } from "../models/user";

@Component({
  selector: 'app-hero-list-all',
  templateUrl: './hero-list-all.component.html',
  styleUrls: ['./hero-list-all.component.css']
})
export class HeroListAllComponent implements OnInit {

  constructor(private heroService: HeroService, private userService: UserService) { }

  heroes: Hero[];
  users: User[];

  ngOnInit() {
    this.getAllHeroes();
  }

  getAllHeroes(): void {
    this.heroService.getAllHeroes()
      .subscribe(r => {
        const result = r as any;
        this.heroes = result.heroes;
      });
  }
}
