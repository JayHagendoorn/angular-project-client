import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroListAllComponent } from './hero-list-all.component';

describe('HeroListAllComponent', () => {
  let component: HeroListAllComponent;
  let fixture: ComponentFixture<HeroListAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroListAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroListAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
