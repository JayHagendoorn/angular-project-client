import { Component, OnInit } from '@angular/core';

import { Weapon } from '../../models/weapon';
import { WeaponService } from '../../../weapon.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-update-weapon',
  templateUrl: './update-weapon.component.html',
  styleUrls: ['./update-weapon.component.css']
})
export class UpdateWeaponComponent implements OnInit {

  constructor(private weaponService: WeaponService,
    private route: ActivatedRoute,
    private router: Router) { }

  name: string = "";
  description: string = "";
  hp: number = 0;
  atk: number = 0;
  spd: number = 0;

  ngOnInit(): void {
    this.getWeapon();
  }

  getWeapon(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.weaponService.getWeapon(id)
      .subscribe(r => {
        const result = r as any;
        const weapon = result.weapon;
        this.name = weapon.name;
        this.description = weapon.description;
        this.hp = weapon.hp;
        this.atk = weapon.atk;
        this.spd = weapon.spd;
      });
  }

  patchWeapon(): void {
    if (this.name != "" &&
      this.description != "" &&
      typeof (this.hp) == "number" &&
      typeof (this.atk) == "number" &&
      typeof (this.spd) == "number") {

      const weapon = new Weapon(this.name, this.description, this.hp, this.atk, this.spd);
      weapon._id = this.route.snapshot.paramMap.get('id');
      this.weaponService.patchWeapon(weapon)
        .subscribe(r => {
          const result = r as any;
          this.router.navigateByUrl(`/weapons`);
        });
    }
  }
}
