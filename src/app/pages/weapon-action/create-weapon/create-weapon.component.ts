import { Component, OnInit } from '@angular/core';

import { Weapon } from '../../models/weapon';
import { WeaponService } from '../../../weapon.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-create-weapon',
  templateUrl: './create-weapon.component.html',
  styleUrls: ['./create-weapon.component.css']
})
export class CreateWeaponComponent implements OnInit {

  constructor(private weaponService: WeaponService,
    private router: Router) { }

  name: string = "";
  description: string = "";
  hp: number = 0;
  atk: number = 0;
  spd: number = 0;

  ngOnInit(): void {
  }

  postWeapon(): void {
    //    const newTemplate = HeroTemplate(this.name, this.title, this.weaponType, this.movementType, this.hp, this.atk, this.spd);
    console.log(this.name);
    if (this.name != "" &&
      this.description != "" &&
      typeof (this.hp) == "number" &&
      typeof (this.atk) == "number" &&
      typeof (this.spd) == "number") {

      const weapon = new Weapon(this.name, this.description, this.hp, this.atk, this.spd);
      this.weaponService.postWeapon(weapon)
        .subscribe(r => {
          const result = r as any;
          this.router.navigateByUrl(`/weapons`);
        });
    }
  }
}
