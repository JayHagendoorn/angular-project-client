import { WeaponType, MoveType } from "./type";

export class HeroTemplate {
  _id: string;
  name: string;
  title: string;
  weaponType: string;
  movementType: string;
  hp: number;
  atk: number;
  spd: number;

  constructor(name: string, title: string, weaponType: string,
    movementType: string, hp: number, atk: number, spd: number) {
    this.name = name;
    this.title = title;
    this.weaponType = weaponType;
    this.movementType = movementType;
    this.hp = hp;
    this.atk = atk;
    this.spd = spd;
  }
}
