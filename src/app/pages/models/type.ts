export enum WeaponType {
  Sword,
  Lance,
  Axe,
  Bow
}

export enum MoveType {
  Infantry,
  Armored,
  Cavalier,
  Flier
}
