import { Hero } from "./hero";

export interface User {
  _id: string;
  username: string;
  password: string;
  heroes: Hero[];
}
