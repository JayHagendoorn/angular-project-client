import { HeroTemplate } from "./hero-template";
import { Weapon } from "./weapon";
import { User } from "./user";

export interface Hero {
  _id: string;
  user: User;
  heroTemplate: HeroTemplate;
  weapon: Weapon;
}
