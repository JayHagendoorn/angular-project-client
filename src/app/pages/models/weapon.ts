import { WeaponType } from "./type";

export class  Weapon {
  _id: string;
  name: string;
  description: string;
  hp: number;
  atk: number;
  spd: number;

  constructor(name: string, description: string, hp: number, atk: number, spd: number) {
    this.name = name;
    this.description = description;
    this.hp = hp;
    this.atk = atk;
    this.spd = spd;
  }
}
