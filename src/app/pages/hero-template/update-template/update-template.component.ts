import { Component, OnInit } from '@angular/core';
import { HeroTemplate } from "../../models/hero-template";
import { UserService } from "../../../user.service";
import { BaseUnitService } from "../../../base-unit.service";
import { ActivatedRoute, Router } from '@angular/router';
import { WeaponService } from "../../../weapon.service";

@Component({
  selector: 'app-update-template',
  templateUrl: './update-template.component.html',
  styleUrls: ['./update-template.component.css']
})
export class UpdateTemplateComponent implements OnInit {

  constructor(private baseUnitService: BaseUnitService,
    private userService: UserService,
    private weaponService: WeaponService,
    private route: ActivatedRoute,
    private router: Router) { }

  name: string = "";
  title: string = "";
  weaponType: string = "";
  movementType: string = "";
  hp: number = 0;
  atk: number = 0;
  spd: number = 0;

  ngOnInit(): void {
    this.getTemplate();
  }

  getTemplate(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.baseUnitService.getHeroTemplate(id)
      .subscribe(r => {
        const result = r as any;
        const template = result.heroTemplate;
        this.name = template.name;
        this.title = template.title;
        this.weaponType = template.weaponType;
        this.movementType = template.movementType;
        this.hp = template.hp;
        this.atk = template.atk;
        this.spd = template.spd;
      });
  }

  patchTemplate(): void {
    if (this.name != "" &&
      this.title != "" &&
      this.weaponType != "" &&
      this.movementType != "" &&
      typeof (this.hp) == "number" &&
      this.hp >= 0 &&
      typeof (this.atk) == "number" &&
      this.atk >= 0 &&
      typeof (this.spd) == "number" &&
      this.spd >= 0) {

      const template = new HeroTemplate(this.name, this.title, this.weaponType, this.movementType, this.hp, this.atk, this.spd);
      template._id = this.route.snapshot.paramMap.get('id');
      this.baseUnitService.patchTemplate(template)
        .subscribe(() => {
          this.router.navigateByUrl(`/templates`);
        });
    }
  }
}
