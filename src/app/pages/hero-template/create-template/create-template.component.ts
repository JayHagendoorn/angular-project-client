import { Component, OnInit } from '@angular/core';
import { BaseUnitService } from "../../../base-unit.service";
import { Router } from "@angular/router";
import { HeroTemplate } from "../../models/hero-template";
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.css']
})
export class CreateTemplateComponent implements OnInit {

  constructor(private baseUnitService: BaseUnitService,
    private router: Router) { }

  name: string = "";
  title: string = "";
  weaponType: string = "";
  movementType: string = "";
  hp: number = 0;
  atk: number = 0;
  spd: number = 0;

  ngOnInit(): void {
  }


  postTemplate(): void {
    if (this.name != "" &&
      this.title != "" &&
      this.weaponType != "" &&
      this.movementType != "" &&
      typeof (this.hp) == "number" &&
      this.hp >= 0 &&
      typeof (this.atk) == "number" &&
      this.atk >= 0 &&
      typeof (this.spd) == "number" &&
      this.spd >= 0) {

      const template = new HeroTemplate(this.name, this.title, this.weaponType, this.movementType, this.hp, this.atk, this.spd);
      this.baseUnitService.postTemplate(template)
        .subscribe(r => {
          const result = r as any;
          this.router.navigateByUrl(`/templates`);
        });
    }
  }
}
