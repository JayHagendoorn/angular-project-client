import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from './layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BaseUnitComponent } from './pages/base-unit/base-unit.component';
import { WeaponComponent } from "./pages/weapon/weapon.component";
import { UserHeroComponent } from "./pages/user-hero/user-hero.component";
import { CreateHeroComponent } from "./pages/create-hero/create-hero.component";
import { HeroListAllComponent } from './pages/hero-list-all/hero-list-all.component';
import { HeroDetailComponent } from './pages/hero-detail/hero-detail.component';
import { CreateTemplateComponent } from './pages/hero-template/create-template/create-template.component';
import { UpdateTemplateComponent } from './pages/hero-template/update-template/update-template.component';
import { CreateWeaponComponent } from './pages/weapon-action/create-weapon/create-weapon.component';
import { UpdateWeaponComponent } from './pages/weapon-action/update-weapon/update-weapon.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
      { path: 'about', pathMatch: 'full', component: UsecasesComponent },

      { path: 'templates', component: BaseUnitComponent },
      { path: 'templates/create', component: CreateTemplateComponent },
      { path: 'templates/:id', component: UpdateTemplateComponent },

      { path: 'weapons', component: WeaponComponent },
      { path: 'weapons/create', component: CreateWeaponComponent },
      { path: 'weapons/:id', component: UpdateWeaponComponent },

      { path: 'myheroes', component: UserHeroComponent },

      { path: 'heroes/create', component: CreateHeroComponent },
      { path: 'heroes', component: HeroListAllComponent },
      { path: 'heroes/:id', component: HeroDetailComponent },
    ],
  },
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: 'register', pathMatch: 'full', component: RegisterComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
