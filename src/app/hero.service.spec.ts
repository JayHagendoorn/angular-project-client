import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HeroService } from './hero.service';
import { Hero } from "./pages/models/hero";

describe('HeroService', () => {
  let heroService: HeroService;
  let httpSpy: { get: jasmine.Spy, post: jasmine.Spy, patch: jasmine.Spy, delete: jasmine.Spy };
  let alertSpy: any;
  let heroServiceSpy: any;
  let routerSpy: any;

  beforeEach(() => {
    alertSpy = jasmine.createSpyObj('AlertService',
      ['error', 'success']);
    httpSpy = jasmine.createSpyObj('HttpClient',
      ['get', 'post', 'patch', 'delete', 'pipe']);
    heroServiceSpy = jasmine.createSpyObj('HeroService',
      ['getHeroes', 'getHero']);
    routerSpy = jasmine.createSpyObj('Router',
      ['navigateByUrl']);

    heroService = new HeroService(httpSpy as any);

  });

  fit('should be created', () => {
    expect(heroService).toBeTruthy();
  });

  fit('should get all heroes after getHeroes()', () => {

//    const subs = heroService.getHeroes().subscribe(r => {
//      const result = r as any;
//      expect(result.heroes.length > 0).toBe(true);
//    });
//
//    expect(true).toBe(true);
//    subs.unsubscribe();
    const expectedTemplates: Hero[] = [
      {
        _id: "1a",
        user: null,
        heroTemplate: null,
        weapon: null
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));
    const subs = heroService.getAllHeroes().subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplates);
    });

    subs.unsubscribe();
  });

  fit('should get specific hero after getHero()', () => {
//    let firstHero: Hero;
//    const stubs = heroService.getHeroes().subscribe(r => {
//      const result = r as any;
//      firstHero = result.heroes[0];
//    });
//
//    const subs = heroService.getHero(firstHero._id).subscribe(r => {
//      const result = r as any;
//      expect(result.hero.heroTemplate).toEqual(firstHero.heroTemplate);
//    });
//
//    subs.unsubscribe();
//    stubs.unsubscribe

    const expectedTemplates: Hero[] = [
      {
        _id: "1a",
        user: null,
        heroTemplate: null,
        weapon: null
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));
    const subs = heroService.getHero(expectedTemplates[0]._id).subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplates);
    });

    subs.unsubscribe();
  });
});
