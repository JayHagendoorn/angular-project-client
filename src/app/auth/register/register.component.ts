import { Component, OnInit } from '@angular/core';
import { UserService } from "../../user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService,
    private router: Router) {
  }
  ngOnInit(): void {
  }

  wrongCreds: boolean = false;
  wrongCredsText: string = "";

  register(username: string, password: string) {
    if (username && password) {
      this.userService.register(username, password)
        .subscribe(
          (r) => {
            if (r != undefined) {
              const result = r as any;
              localStorage.setItem("bearerToken", result.info.token);
              localStorage.setItem("AUsername", result.info.username);
              localStorage.setItem("AUserId", result.info.id);
              this.router.navigateByUrl('/');
            } else {
              this.wrongCreds = true;
              this.wrongCredsText = "User already exists";
            }
          }
        );
    } else {
      this.wrongCreds = true;
      this.wrongCredsText = "Please fill in the forms";
    }
  }
}
