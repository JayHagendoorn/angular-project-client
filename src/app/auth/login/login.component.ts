import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from "../../user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private userService: UserService,
    private router: Router) {
  }
  ngOnInit(): void {}

  wrongCreds: boolean = false;
  wrongCredsText: string = "";

  login(username: string, password: string) {
    if (username && password) {
      this.userService.login(username, password)
        .subscribe(
          (r) => {
            if (r != undefined) {
              const result = r as any;
              localStorage.setItem("bearerToken", result.info.token);
              localStorage.setItem("AUsername", result.info.username);
              localStorage.setItem("AUserId", result.info.id);
              this.router.navigateByUrl('/');
            } else {
              this.wrongCreds = true;
              this.wrongCredsText = "Username or password incorrect";
            }
          }
      );
    } else {
      this.wrongCreds = true;
      this.wrongCredsText = "Please fill in the forms";
    }
  }
}
