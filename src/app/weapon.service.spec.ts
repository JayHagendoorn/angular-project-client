import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { WeaponService } from './weapon.service';
import { Weapon } from "./pages/models/weapon";

describe('WeaponService', () => {
  let weaponService: WeaponService;
  let httpSpy: { get: jasmine.Spy, post: jasmine.Spy, patch: jasmine.Spy, delete: jasmine.Spy };
  let alertSpy: any;
  let weaponServiceSpy: any;
  let routerSpy: any;

  beforeEach(() => {
    alertSpy = jasmine.createSpyObj('AlertService',
      ['error', 'success']);
    httpSpy = jasmine.createSpyObj('HttpClient',
      ['get', 'post', 'patch', 'delete', 'pipe']);
    weaponServiceSpy = jasmine.createSpyObj('WeaponService',
      ['getWeapons', 'getWeapon']);
    routerSpy = jasmine.createSpyObj('Router',
      ['navigateByUrl']);

    weaponService = new WeaponService(httpSpy as any);

  });

  fit('should be created', () => {
    expect(weaponService).toBeTruthy();
  });

  fit('should get all weapons after geteapons()', () => {
//    const subs = weaponService.getWeapons().subscribe(r => {
//      const result = r as any;
//      expect(result.weapons.length > 0).toBe(true);
//    });
//
    //    subs.unsubscribe();
    const expectedTemplates: Weapon[] = [
      {
        _id: "1a",
        name: "NewTest",
        description: "NewTitle",
        hp: 2,
        atk: 2,
        spd: 2
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));
    const subs = weaponService.getWeapons().subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplates);
    });

    subs.unsubscribe();
  });

  fit('should get specific weapon after getWeapon()', () => {
//    let firstWeapon: Weapon;
//    const stubs = weaponService.getWeapons().subscribe(r => {
//      const result = r as any;
//      firstWeapon = result.weapons[0];
//    });
//
//    const subs = weaponService.getWeapon(firstWeapon._id).subscribe(r => {
//      const result = r as any;
//      expect(result.weapon.name).toEqual(firstWeapon.name);
//    });
//
//    stubs.unsubscribe();
    //    subs.unsubscribe();
    const expectedTemplates: Weapon[] = [
      {
        _id: "1a",
        name: "NewTest",
        description: "NewTitle",
        hp: 2,
        atk: 2,
        spd: 2
      }
    ];
    httpSpy.get.and.returnValue(of(expectedTemplates));
    const subs = weaponService.getWeapon(expectedTemplates[0]._id).subscribe(r => {
      const result = r as any;
      expect(result).toEqual(expectedTemplates);
    });

    subs.unsubscribe();
  });
});
