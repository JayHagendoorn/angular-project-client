import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  let expectedTitle = 'My Hero';

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display the expected title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual(expectedTitle);
  });

//  it('should weapons return more than 1', () => {
//    page = new 
//  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
