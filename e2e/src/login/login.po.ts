import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/login") as Promise<unknown>;
  }

  login() {
    this.setCredentials("admin", "admin");
    this.clickLoginSubmit();
  }

  clickLoginSubmit() {
    element(by.css("button[type=submit]")).click();
  }

  setCredentials(username: string, password: string) {
    element(by.className("new_username_input")).sendKeys(username);
    element(by.className("new_password_input")).sendKeys(password);
  }
}
