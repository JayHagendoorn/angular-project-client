import { UpdateWeaponPage } from './update-weapon.po';
import { LoginPage } from '../login/login.po';
import { browser, by, element, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: UpdateWeaponPage;
  let loginPage: LoginPage;
  const newDescription = `This is a new description at ${new Date()}!`;

  beforeEach(() => {
    page = new UpdateWeaponPage();
    loginPage = new LoginPage();
  });

  it('should update weapon ',
    () => {
      loginPage.navigateTo();
      browser.driver.sleep(2000);
      loginPage.login();
      browser.driver.sleep(2000);

      page.navigateTo();
      browser.driver.sleep(500);

      page.updateDescription(newDescription);
      browser.driver.sleep(2000);
      page.navigateTo();
      browser.driver.sleep(500);
      expect(page.getDescription()).toBe(newDescription);
    });
});
