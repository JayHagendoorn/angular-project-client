import { browser, by, element } from 'protractor';

export class UpdateWeaponPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/weapons/607453fc374aaa00228dda83") as Promise<unknown>;
  }

  updateDescription(newDescription: string) {
    element(by.id("description")).clear();
    element(by.id("description")).sendKeys(newDescription);
    element(by.css("button[type=submit]")).click();
  }

  getDescription() {
    return element(by.id("description")).getAttribute('value');
  }
}
