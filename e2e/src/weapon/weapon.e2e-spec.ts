import { WeaponPage } from './weapon.po';
import { browser, logging } from 'protractor';
import { LoginPage } from '../login/login.po';

describe('workspace-project App', () => {
  let page: WeaponPage;
  let loginPage: LoginPage;
  let expectedTitle = 'My Hero';

  beforeEach(() => {
    page = new WeaponPage();
    loginPage = new LoginPage();
  });

  it('should weapons return more than 1',
    () => {
      page.navigateTo();
      browser.driver.sleep(500);
      expect(page.getWeaponAmount()).toBeGreaterThan(0);
    });

//  it('should update weapon ', () => {
//    loginPage.login();
//    browser.driver.sleep(2000);
//
//    page.navigateTo();
//    browser.driver.sleep(500);
//    expect(page.getWeaponAmount()).toBeGreaterThan(0);
//  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
