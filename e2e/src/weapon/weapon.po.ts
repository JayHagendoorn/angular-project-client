import { browser, by, element } from 'protractor';

export class WeaponPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/weapons") as Promise<unknown>;
  }

  getWeaponAmount() {
    return element.all(by.css('.weapon-holder')).count();
  }
}
