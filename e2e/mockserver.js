const express = require("express");
const post = 3000;

let app = express();
let routes = express.Router;

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*")
  res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Content-Length, Accept, token");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

routes.post("/api/users/login",
  (req, res, next) => {
    res.status(200).json({
      info: {
        token:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNjRjN2ZmYTYwZDkxMjMzNDE0NTE4YiIsImlhdCI6MTYxODQwNzQ4MSwiZXhwIjoxNjIwMTM1NDgxfQ.eJ2jYKLSIKzROmz45pXU78MPTasF4278rJJbL8rK4mM",
        username: "admin",
        id: "6064c7ffa60d91233414518b"
      }
    });
  });

routes.get("/api/templates",
  (req, res, next) => {
    res.status(200).json({
      heroTemplates: [
        { _id: "6064444aaf7bfe592070f88d",
          name: "Liz",
          title: "Spright",
          weaponType: "Axe",
          movementType: "Infantry",
          hp: 30,
          atk: 25,
          spd: 23
        },
        {
          _id: "6068d07a20f1c60022d678ff",
          name: "Rube",
          title: "Stale",
          weaponType: "Bow",
          movementType: "Infantry",
          hp: 40,
          atk: 27,
          spd: 18
        }]
    });
  });

routes.patch("/api/templates/:templateId",
  (req, res, next) => {
    res.status(200).json({
      heroTemplate:
        {
          _id: "6064444aaf7bfe592070f88d",
          name: "Liz",
          title: "Spright",
          weaponType: "Axe",
          movementType: "Infantry",
          hp: 30,
          atk: 25,
          spd: 23
        }
    });
  });

app.use('*', function(req, res, next) {
  next({ error: "Endpoint does not exist"})
})

app.use((err, req, res, next) => {
  res.status(400).json(err);
})


app.listen(port, () => {
  console.log(`Mock API listening on post ${port}`, port)
})
